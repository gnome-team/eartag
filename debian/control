Source: eartag
Section: misc
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Matthias Geiger <werdahias@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 meson (>= 0.56.0),
 blueprint-compiler,
 dh-sequence-python3,
 libadwaita-1-dev (>= 1.5.0),
 libglib2.0-dev,
 libgdk-pixbuf-2.0-dev,
 python3-acoustid,
 python3-magic,
 python3-mutagen,
 python3-gi,
 python3-pil,
 python3-pytest,
Standards-Version: 4.7.0
Homepage: https://gitlab.gnome.org/World/eartag
Vcs-Browser: https://salsa.debian.org/gnome-team/eartag
Vcs-Git: https://salsa.debian.org/gnome-team/eartag.git

Package: eartag
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 gir1.2-adw-1 (>= 1.5.0),
 gir1.2-gtk-4.0 (>= 4.16),
 python3-acoustid,
 python3-gi,
 python3-magic,
 python3-mutagen,
 python3-pil,
Description: Edit audio file tags
 Ear Tag is a simple audio file tag editor. It is primarily geared towards
 making quick edits or bulk-editing tracks in albums/EPs.
 Unlike other tagging programs, Ear Tag does not require the user to set up
 a music library folder.
 .
 Ear Tag supports both desktop and mobile form factors.
 .
 Ear Tag is a GNOME Circle app.
